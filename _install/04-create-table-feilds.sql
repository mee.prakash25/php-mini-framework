CREATE TABLE `mini`.`fields` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `field1` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
   `field2` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
   `field3` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
