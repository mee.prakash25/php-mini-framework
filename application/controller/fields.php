<?php

class Fields extends Controller
{
    public function index()
    {
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/fields/index.php';
        require APP . 'view/_templates/footer.php';
    }
}